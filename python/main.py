import json
import socket
import sys
import copy

class PieceGamePlan(object):

    def __init__(self, p):
        self.speeds = dict()
        self.piece_def = p
        self.next_corner = [-1, dict()]
        self.dist_to_next_corner = 0

    def find_speed(self, dist):
        prev = 0
        for i, v in self.speeds.iteritems():
            if dist > i:
                return prev
            prev = v
        return prev

    def set_speed(self, dist, speed):
        self.speeds[dist] = speed

    def __str__(self):
        return str(self.speeds)


class SpeedGet(object):

    opt_speed_radius_200 = 0.74
    opt_speed_radius_100 = 0.61
    opt_speed_radius_less_100 = 0.5
    
    def __init__(self, i, m4, m3, m2, m1):
        self.current_speed = 0.0
        self.summ_speed = 0
        self.corner_length = 0
        self.corner_after = 0
        self.next_corner_radius = 0
        self.speed = 0.0
        self.current_radius = 0
        self.next_straigth_index = 4
        self.next_is_straight = 0
        if i != 0 and i != 1 and i != 2 and i != 3:
            self.minussFour = m4
           # self.summ_speed = self.summ_speed + pieceGamePlans[self.minussFour]
        if i != 0 and i != 1 and i != 2:
          #  self.minussThree = m3
            self.summ_speed = self.summ_speed + m3
        else:
            self.summ_speed = self.summ_speed + 1
        if i != 0 and i != 1:
           # self.minussTwo = m2
            self.summ_speed = self.summ_speed + m2
        else:
            self.summ_speed = self.summ_speed + 1
        if i != 0:
          #  self.minussOne = m1
            self.summ_speed = self.summ_speed + m1
        else:
            self.summ_speed = self.summ_speed + 1
        
    def find_correct_speed(self, pieces, i):
        if (i + 4) < len(pieces):
            self.plussFour = i + 4
            if "radius" in pieces[self.plussFour]:
                self.corner_length = self.corner_length + 1
                self.corner_after = 4
                self.next_corner_radius = pieces[self.plussFour]["radius"]
            else:
                self.next_straigth_index = self.next_straigth_index -1
        else:
            self.next_straigth_index = self.next_straigth_index -1
        if (i + 3) < len(pieces):
            self.plussThree = i + 3
            if "radius" in pieces[self.plussThree]:
                self.corner_length = self.corner_length + 1
                self.corner_after = 3
                self.next_corner_radius = pieces[self.plussThree]["radius"]
            else:
                self.next_straigth_index = self.next_straigth_index -1
        else:
            self.next_straigth_index = self.next_straigth_index -1
        if (i + 2) < len(pieces):
            self.plussTwo = i + 2
            if "radius" in pieces[self.plussTwo]:
                self.corner_length = self.corner_length + 1
                self.corner_after = 2
                self.next_corner_radius = pieces[self.plussTwo]["radius"]
            else:
                self.next_straigth_index = self.next_straigth_index -1
        else:
            self.next_straigth_index = self.next_straigth_index -1
        if (i + 1) < len(pieces):
            self.plussOne = i + 1
            if "radius" in pieces[self.plussOne]:
                self.corner_length = self.corner_length + 1
                self.corner_after = 1
                self.next_corner_radius = pieces[self.plussOne]["radius"]
            else:
                self.next_straigth_index = self.next_straigth_index -1
        else:
            self.next_straigth_index = self.next_straigth_index -1
              
                
        self.current_speed = self.summ_speed / 3
        if "radius" in pieces[i]:
            self.current_radius = pieces[i]["radius"]
                
        print("Speed #{0} at {1}".format(self.speed, 1))
        if self.corner_after >= 3 and self.current_radius < 1:
            self.speed = 1.0
            print("Speed #{0} at {1}".format(self.speed, 2))
            
        elif self.corner_after > 1 and self.current_radius > 1:
        #>=200 radius
            if self.current_radius >= 200:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_200 + 0.1
                else:
                    self.speed = self.opt_speed_radius_200 + 0.2
        #>=100 radius
            elif self.current_radius >= 100 and self.next_corner_radius >= 100:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_100
                else:
                    self.speed = self.opt_speed_radius_100
        #<100 radius
            elif self.current_radius < 100 or self.next_corner_radius < 100:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_less_100 - 0.1
                else:
                    self.speed = self.opt_speed_radius_less_100  
                    
                
            print("Speed #{0} at {1}".format(self.speed, 4))
        elif self.corner_after > 1:
            #>=200 radius
            if self.next_corner_radius >= 200:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_200 + 0.1
                else:
                    self.speed = self.opt_speed_radius_200 + 0.2
        #>=100 radius
            elif self.next_corner_radius >= 100:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_100 - 0.2
                else:
                    self.speed = self.opt_speed_radius_100 + 0.1
        #<100 radius
            elif self.next_corner_radius < 100:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_less_100 - 0.3
                else:
                    self.speed = self.opt_speed_radius_less_100 
                   
            print("Speed #{0} at {1}".format(self.speed, 5))

        elif self.corner_after is 1 and self.current_radius > 1:
        #>=200 radius
            if self.current_radius >= 200 and self.next_corner_radius >= 200:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_200
                else:
                    self.speed = self.opt_speed_radius_200 + 0.1
        #>=100 radius
            elif self.current_radius >= 100 and self.next_corner_radius >= 100:
                if self.current_speed > self.opt_speed_radius_100:
                    self.speed = 0.0
                else:
                    self.speed = self.opt_speed_radius_100
        #<100 radius
            elif self.current_radius < 100 or self.next_corner_radius < 100:
                    self.speed = self.opt_speed_radius_less_100 - 0.1
                    
            print("Speed #{0} at {1}".format(self.speed, 6))
            
        elif self.corner_after is 1:
            #>=200 radius
            if self.next_corner_radius >= 200:
                self.speed = self.opt_speed_radius_200 + 0.1
            #>=100 radius
            elif self.next_corner_radius >= 100:
                if self.current_speed > self.opt_speed_radius_100:
                    self.speed = 0.0
                else:
                    self.speed = self.opt_speed_radius_100
            #<100 radius
            elif self.next_corner_radius < 100:
                if self.current_speed > self.opt_speed_radius_less_100 or (self.corner_length > 2 and self.current_speed > self.opt_speed_radius_less_100 - 0.3):
                    self.speed = 0.0
                else:
                    self.speed = self.opt_speed_radius_less_100
            print("Speed #{0} at {1} next corner {2}, current speed {3}".format(self.speed, 7, self.next_corner_radius, self.current_speed))
            
        elif self.current_radius > 1:
            #>=200 radius
            if self.current_radius >= 200:
                if self.corner_length > 2:
                    self.speed = self.opt_speed_radius_200
                else:
                    self.speed = self.opt_speed_radius_200 + 0.1
        #>=100 radius
            elif self.current_radius >= 100:
                    if self.corner_after > 1:
                        self.speed = self.opt_speed_radius_100 + 0.1
                    else:
                        self.speed = self.opt_speed_radius_100
        #<100 radius
            elif self.current_radius < 100:
                self.speed = self.opt_speed_radius_less_100
            
            print("Speed #{0} at {1}".format(self.speed, 8))
            
        print("Speed #{0} at {1}".format(self.speed, 9))
        
        if self.next_straigth_index is 0 or self.next_straigth_index is 1:
            self.speed = 1.0

        print("Speed #{0} at {1} , radius {2}, piece {3}".format(self.speed, 10, self.current_radius, i))
        return self.speed


class GamePlan(object):
    pieceGamePlans = []

    def __init__(self, pieces):
        for i, p in enumerate(pieces):
            pgp = PieceGamePlan(p)
            for j, pp in enumerate(pieces[i+1:]):
                if "radius" in pp:
                    pgp.next_corner = [i+j+1, pp]
                    break
                pgp.dist_to_next_corner += pp["length"]
            #print("Piece #{}: Next corner {} in {}m".format(i, next_corner, dist_to_next_corner))
            #if i != 0 and i != 1 and ((i + 2) < len(pieces)) and "radius" in pieces[i + 2] and "radius" not in pieces[i + 1] and "radius" not in pieces[i - 1] and "radius" not in pieces[i - 2]:
             #   pgp.set_speed(0, 0.4)
            #elif ((i + 1) < len(pieces)) and "radius" in pieces[i + 1]:
             #   pgp.set_speed(0, 0.4)
            #elif "length" in p:
                # Straight piece
             #   pgp.set_speed(0, 0.7)
            #else:
                # Corner
            m4 = 0
            m3 = 0
            m2 = 0
            m1 = 0    
                
            if i != 0 and i != 1 and i != 2 and i != 3:
                m4 = self.pieceGamePlans[i - 4].speeds[0]
            if i != 0 and i != 1 and i != 2:
                m3 = self.pieceGamePlans[i - 3].speeds[0]
            if i != 0 and i != 1:
                m2 = self.pieceGamePlans[i - 2].speeds[0]
            if i != 0:
                m1 = self.pieceGamePlans[i - 1].speeds[0]
                
                
            speedGet = SpeedGet(i, m4, m3, m2, m1)
            
            pgp.set_speed(0, speedGet.find_correct_speed(pieces, i))

            self.pieceGamePlans.append(pgp)

    def __str__(self):
        s = ""
        for i in range(len(self.pieceGamePlans)):
            s = s + "Piece #" + str(i) + ": " + str(self.pieceGamePlans[i]) + "\n"
        return s

class NoobBot(object):

    track_data = None
    game_plan = None

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.current_throttle = 0.0
        self.piece_idx = 0
        self.current_lane = 0
        self.wanted_lane = 0
        self.lane_switch = "None"

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})
        #return self.send("{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \"" + self.name + "\", \"key\": \"Xiga9y9yRmoC0Q\" }, \"trackName\": \"france\", \"carCount\": 1 }}")

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
      #  print(data)
        self.track_data = data
        self.game_plan = GamePlan(data["race"]["track"]["pieces"])
        #print(data)
        print(self.game_plan)
 

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        #print(data)
        for car_data in data:
            if car_data["id"]["name"] == self.name:
                self.piece_idx = car_data["piecePosition"]["pieceIndex"]
                pgp = self.game_plan.pieceGamePlans[self.piece_idx]

                self.current_lane = car_data["piecePosition"]["lane"]["startLaneIndex"]
                if self.current_lane == self.wanted_lane:
                    self.lane_switch = "None"

                wanted_throttle = pgp.speeds[0]
                if self.current_throttle != wanted_throttle:
                    #print("Changing throttle at piece #{0} from {1} to {2}".format(car_data["piecePosition"]["pieceIndex"], self.current_throttle, wanted_throttle))
                    self.current_throttle = wanted_throttle
                if pgp.next_corner[0] != -1 and "switch" in self.game_plan.pieceGamePlans[(self.piece_idx + 1) % len(self.game_plan.pieceGamePlans)].piece_def:
                    if pgp.next_corner[1]["angle"] > 0 and self.lane_switch != "Right":
                        #print("Switching lane to right", self.piece_idx, pgp.next_corner, self.lane_switch)
                        self.msg("switchLane", "Right")
                        self.wanted_lane = self.current_lane + 1
                        self.lane_switch = "Right"
                    elif pgp.next_corner[1]["angle"] < 0 and self.lane_switch != "Left":
                        #print("Switching lane to left", self.piece_idx, pgp.next_corner, self.lane_switch)
                        self.msg("switchLane", "Left")
                        self.wanted_lane = self.current_lane - 1
                        self.lane_switch = "Left"
                    else:
                        self.throttle(wanted_throttle)
                else:
                    self.throttle(wanted_throttle)

    def on_crash(self, data):
        if data["name"] == self.name:
            #print("We crashed. Need to recalculate game plan!")

            SpeedGet.opt_speed_radius_200 -= 0.1
            SpeedGet.opt_speed_radius_100 -= 0.1
            SpeedGet.opt_speed_radius_less_100 -= 0.1

            pgps = self.game_plan.pieceGamePlans
            lp = len(self.track_data["race"]["track"]["pieces"])
            for i, p in enumerate(self.track_data["race"]["track"]["pieces"]):
                speedGet = SpeedGet(i, pgps[(lp+i-4)%lp].speeds[0], pgps[(lp+i-3)%lp].speeds[0], pgps[(lp+i-2)%lp].speeds[0], pgps[(lp+i-1)%lp].speeds[0])
                
                pgps[i].set_speed(0, speedGet.find_correct_speed(self.track_data["race"]["track"]["pieces"], i))

        else:
            print("Someone else crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended", data)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_available(self, data):
        print("Got turboAvailable", data)
        pass

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_available,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
